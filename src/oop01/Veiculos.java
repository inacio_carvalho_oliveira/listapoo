package oop01;

public class Veiculos 
{
   
    private  String modelo;
    private  String cor;
    private  String marca;
    private  double preco;  

    public Veiculos(){}   
    public Veiculos(String cor, String modelo, String marca, Double preco)
    {
       
        this.modelo = modelo;
        this.cor    = cor;
        this.marca  = marca;
        this.preco  = preco;
    }

    /**
     * @return String return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     * @return String return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return String return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return double return the preco
     */
    public double getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
	public String toString() {
		return (
				 this.getMarca()  +", "+
                 this.getCor()    +", "+
				 this.getModelo() +", "+
				 this.getPreco()  
				    
				);
	}


}
