package P1;

import java.nio.file.ClosedWatchServiceException;
import java.util.Random;
import java.util.Scanner;

import javax.print.attribute.standard.MediaSize;

public class Atividades21_30 {
    public static void main(String[] args) {
        //Ex21();
        //Ex22();
        //Ex23();
        //Ex24A();
        //Ex24B();
        //Ex24C();
        //Ex25();
        Ex26();
        //Ex27();
    }
    public static void Ex21() {
        float a = 3, b = 2, c = -5;
        double quadrado = Math.pow(b, 2);
        double delta = quadrado - 4 * a * c;
        if (delta >= 0) {
            System.out.printf("Delta: %.2f", delta);
            double xpos = (-b + (Math.sqrt(delta)));
            double xneg = (-b - (Math.sqrt(delta)));
            System.out.printf("\nXI: %.2f", xpos / (2 * a));
            System.out.printf("\nXII: %.2f", xneg / (2 * a));
        } else System.out.printf("Não há raizes negativas");
        System.out.printf("\n");
    }

    public static void Ex22() {
        float Po = 0, v = 0, a = 0;
        int t = 0;
        double quadrado = Math.pow(t, 2);
        double pf = Po + (v * t) + (a * quadrado) / (2);
        System.out.printf("PF : %.2f", pf);
    }

    public static void Ex23() {
        float pv = 0, j = 0, i = 0;
        int n = 0;
        double quadrado = Math.pow(i, n);
        double fv = (pv * (1 + j)) * (quadrado);
        System.out.printf("FV: %.2f", fv);
        System.out.println("\n");
    }

    public static void Ex24A(){
        int num = 2;
        for(int i = 0; i <= 20; i++) {
            Integer resultado = num * i;
            System.out.println(num + " X " + i + " = " + resultado);
        }
    }
    public static void Ex24B() {
        int entrada;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informar um número: ");
		entrada = scanner.nextInt();
		int multiplicador = 0;
		do {
			int resultado = entrada * multiplicador;
			System.out.println(resultado);
			multiplicador++;
		} while (multiplicador <= 20);


    }
    public static void Ex24C(){
        int entrada;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informar um número: ");
		entrada = scanner.nextInt();

		int multiplicador = 0;
		while (multiplicador <= 20) {
			int resultado = entrada * multiplicador;
			System.out.println(resultado);
			multiplicador++;
		}
    }
    public static void Ex25(){
        Random aleatorio = new Random();
        int lim =5;
        for (int i = 1; i<=lim;i++) {
            int valor = aleatorio.nextInt(30);
            System.out.println("Número gerado: " + valor);
        }
    }
    public static void Ex26(){
        Scanner sc = new Scanner(System.in);
		int start = 1;
		System.out.println("1 - inserir numero || 0 - sair");
		int escolha = sc.nextInt();

		while (escolha == 1) {
			do {
				System.out.print("Informe um número : ");
				int[] numero = new int[] { sc.nextInt() };
				System.out.println("1 - inserir numero || 0 - sair");
				escolha = sc.nextInt();
				if (escolha == 0) {
					for (int i = 0; i < numero.length; i++) {
						for (int j = 0; i < numero.length; j++) {
							System.out.println("value: " + (numero[i] + numero[j]));
							break;
						}
					}
				}
			} while (start == 1);
		}

    }

    public static void Ex27() {
		/*
		 * Calcule a media de Notas de alunos. O programa deve parar de ler valores
		 * quando for fornecido um valor negativo como nota.
		 */
		Scanner sc = new Scanner(System.in);
		int resposta,respostaEscolha;
        int qtdAlunos;
        float respostaNotaA =0,
              respostaNotaB =0,
              respostaNotaC =0; 
        
		System.out.println("O que deseja fazer?\n1 - calculo de media  ||  0 - sair ");
		System.out.print("Sua escolha : ");
		resposta = sc.nextInt();
		
		switch (resposta) {
		case 1: 
			int alunos = 0;
			System.out.printf("Quantos alunos a serem avaliados:");
			qtdAlunos = sc.nextInt();
			for (int j = 0; j <= qtdAlunos;j++) {
			do {
				for( int i = 0; i <= qtdAlunos; i++) {
										
					System.out.printf("Nota do aluno:"  );					
					respostaNotaA = sc.nextInt();
					System.out.printf("Nota do aluno:"  );					
					respostaNotaB = sc.nextInt();
					System.out.printf("Nota do aluno:"  );					
					respostaNotaC = sc.nextInt();
					System.out.println("1-Ver media || 2-Continuar 0 - Sair");
					respostaEscolha = sc.nextInt();
					if (respostaEscolha ==2 ){ continue;}           
					if (respostaEscolha == 1) {
						float[] media = new float[3];
                        media[0] = respostaNotaA;
                        media[1] = respostaNotaB;
                        media[2] = respostaNotaC;
                        float mediaAritimetica = (media[0]+media[1]+media[2]) / 3;
                            System.out.println("valores : "+mediaAritimetica);break;

					} else {
						System.out.println("voce decidiu sair bye");
					}
                
                }
            }while(qtdAlunos <= 0);		
            System.out.println("voce decidiu sair bye");break;
		}
	}
}}
