package P1;
import java.util.Scanner;
public class Atividades1_10 {
    public static void main(String[] args) {
        //Ex1();
        //Ex2();
        //Ex3();
        //Ex4();
        //Ex5();
        //Ex6();
        //Ex7();
        //Ex8e9();
        //Ex10();
    }
        public static void Ex1 () {
            Scanner sc = new Scanner(System.in);
            System.out.println("Informe um número inteiro: ");
            int numero = sc.nextInt();
            System.out.println("o numero inteiro digitado foi: " + numero);
        }
        public static void Ex2 () {
            Scanner sc = new Scanner(System.in);
            System.out.println("Informe um número Real: ");
            float numero = sc.nextFloat();
            System.out.println("o numero inteiro digitado foi: " + numero);
        }
        public static void Ex3 () {
            Scanner sc = new Scanner(System.in);
            System.out.println("Informe um número qualquer: ");
            float numero = sc.nextFloat();
            while (numero > 50) {
                System.out.println("Número :" + numero + " maior que 50");
                break;
            }
        }
        public static void Ex4 () {
            Scanner sc = new Scanner(System.in);
            System.out.println("Informe um número qualquer: ");
            float numero = sc.nextFloat();
            while (numero > 100) {
                System.out.println("Número :" + numero + " maior que 100");
                break;
            }
            System.out.println("Número :" + numero + " menor que 100");
        }
        public static void Ex5 () {
            Scanner sc = new Scanner(System.in);
            System.out.println("informe um número inteiro qualquer");
            int numero = sc.nextInt();
            if (numero % 2 == 0) {
                System.out.println("Número: " + numero + " é Par");
            } else if (numero % 2 == 1) {
                System.out.println("Número: " + numero + " é ìmpar");
            }
        }
        public static void Ex6 () {
            /**
             *
             * Escreva um programa que leia dois valores, os quais denominaremos A e B, fornecidos pelo usuário, exibindo respostas
             * F ou V para as seguintes questões:
             * (a) A é maior que zero?
             * (b) B é maior que zero;
             * (c) A E B são maiores do que zero?
             * Use operadores lógicos para formular as expressões necessárias a avaliação dos valores.
             */
            int a = 1, b = 0;
            if (a > 0) {
                System.out.println("A maior que Zero");
            }
            if (b < 0) {
                System.out.println("B moior que zero");
            }
            if (a > 0 && b > 0) {
                System.out.println("a e b são maiores que Zero");
            }
        }
        public static void Ex7 () {
            /**
             Escreva um programa que leia dois valores, os quais denominaremos A e B, fornecidos pelo usuário, exibindo respostas
             F ou V para as seguintes questões:
             (a) A é igual a zero?
             (b) B é menor que zero; e
             (c) A OU B são maiores do que zero?
             Use operadores lógicos para formular as expressões necessárias a avaliação dos valores.
             */
            int a = 0, b = -1;
            if (a == 0) {
                System.out.println("A igual que Zero");
            }
            if (b < 0) {
                System.out.println("B menor que zero");
            }
            if (a > 0 && b > 0) {
                System.out.println("a e b são maiores que Zero");
            }
        }
        public static void Ex8e9 () {
            int[] num = {3, 6, 12};
            float min = num.length, max = num.length;
            for (float count : num) {
                if (count < min) min = count;
                else if (count > max) max = count;
            }
            System.out.printf("Min: %.2f\nMax: %.2f", min, max);
        }
        public static void Ex10 () {

            Scanner sc = new Scanner(System.in);
            System.out.print("Digite um valor para A: ");
            int a = sc.nextInt();
            System.out.print("Digite um valor para B: ");
            int b = sc.nextInt();
            System.out.print("Digite um valor para C: ");
            int c = sc.nextInt();

            if ((a > b) && (b > c)) {
                System.out.print("Ordem: " + a + "> " + b + ">" + c);
            } else {
                if ((a > c) && (c > b)) {
                    System.out.print("Ordem: " + a + "> " + c + ">" + b);
                } else {
                    if ((b > a) && (a > c)) {
                        System.out.print("Ordem: " + b + "> " + a + ">" + c);
                    } else {
                        if ((b > c) && (c > a)) {
                            System.out.print("Ordem: " + b + "> " + c + ">" + a);
                        } else {
                            if ((c > a) && (a > b)) {
                                System.out.print("Ordem: " + c + "> " + a + ">" + b);
                            } else {
                                // só sobrou ( (c>b) && (b>a))
                                System.out.print("Ordem: " + c + "> " + b + ">" + a);
                            }
                        }
                    }
                }
            }

        }

    }
